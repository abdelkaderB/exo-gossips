package com.nespresso.recruitment.gossip;

import java.util.ArrayList;
import java.util.List;

public class UnerrazableGossipStrategie implements GossipStrategie {
	private final List<String> messages;
	private int lastGossipedMessage;

	public UnerrazableGossipStrategie() {
		this.messages = new ArrayList<String>();
		lastGossipedMessage = 0;
	}

	@Override
	public void memorizeThis(String message, GossipPerson from) {
		messages.add(message);
	}

	@Override
	public String getAllRetainedMessages() {
		final StringBuilder messagesBuilder = new StringBuilder();
		for (String message : messages) {
			messagesBuilder.append(message).append(", ");
		}
		int deleteFromIndex = messagesBuilder.length() - 2;
		return messagesBuilder.delete(deleteFromIndex, deleteFromIndex + 2).toString();
	}

	@Override
	public String getLastMessage() {
		return messages.get(lastGossipedMessage++);
	}

	@Override
	public boolean hasMessageToSpread() {
		return messages.size() > lastGossipedMessage;
	}

}
