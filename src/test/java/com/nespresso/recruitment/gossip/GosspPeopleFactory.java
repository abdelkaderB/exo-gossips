package com.nespresso.recruitment.gossip;

public class GosspPeopleFactory {

	public static GossipPerson create(String title) {
		final GossipPerson createdGossipPerson;
		switch (title) {
		case "Mr":
			createdGossipPerson = new GossipPerson(Titles.MR, new ErrazableGossipStrategie());
			break;
		case "Dr":
			createdGossipPerson = new GossipPerson(Titles.DR, new UnerrazableGossipStrategie());
			break;
		case "Sir":
			createdGossipPerson = new GossipPerson(Titles.SIR, new GentelmenGossipStrategie());
			break;
		case "Agent":
			createdGossipPerson = new GossipPerson(Titles.AGENT, new AgentGossipStrategie());
			break;
		case "Pr":
			createdGossipPerson = new GossipPerson(Titles.PROFESSOR, new ProfessorGossipStrategie());
			break;
		case "Lady":
			createdGossipPerson = new GossipPerson(Titles.LADY, new LadyGossipStrategie());
			break;
		default:
			createdGossipPerson = new GossipPerson(Titles.OTHERS, null);
		}
		createdGossipPerson.canTalkTo(new GossipPerson(Titles.OTHERS, null));
		return createdGossipPerson;
	}

}
