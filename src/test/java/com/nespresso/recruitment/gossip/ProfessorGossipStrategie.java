package com.nespresso.recruitment.gossip;

public class ProfessorGossipStrategie extends ErrazableGossipStrategie {
	private int turnDelayed;

	public ProfessorGossipStrategie() {
		this.turnDelayed = 0;
	}

	@Override
	public String getLastMessage() {
		if (turnDelayed <= 0 && super.hasMessageToSpread()) {
			turnDelayed++;
			return "";
		}
		return super.getLastMessage();
	}

}
