package com.nespresso.recruitment.gossip;

public class AgentGossipStrategie extends ErrazableGossipStrategie {
	@Override
	public void memorizeThis(String message,GossipPerson from) {
		String allRetainedMessages = super.getAllRetainedMessages();
		if (allRetainedMessages.equals(""))
			super.memorizeThis(message,from);
		else
			super.memorizeThis(allRetainedMessages + ", " + message,from);
	}
	@Override
	public String getLastMessage() {
		super.errazeMemory();
		return super.getLastMessage();
	}
}
