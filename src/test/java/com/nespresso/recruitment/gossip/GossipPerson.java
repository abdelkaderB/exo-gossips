package com.nespresso.recruitment.gossip;

import java.util.List;

public class GossipPerson {
	private final Titles title;
	private final GossipStrategie gossip;
	private GossipPerson gossipePersonToTalkWith;

	public GossipPerson(Titles title, GossipStrategie longTermeMemory) {
		this.title = title;
		this.gossip = longTermeMemory;
	}

	public void canTalkTo(GossipPerson gossipePersonToTalkWith) {
		this.gossipePersonToTalkWith = gossipePersonToTalkWith;
	}

	public String ask() {
		return gossip.getAllRetainedMessages();
	}

	public void listenToThat(String wordToSay, GossipPerson from) {
		this.gossip.memorizeThis(wordToSay, from);
		if (title.equals(Titles.SIR))
			gossipePersonToTalkWith = from;
	}

	public GossipPerson gossipeNow(List<GossipPerson> peopleAlreadyHaveAGossip) {
		if (isTheListenerIlligibleToHearTheGossip(peopleAlreadyHaveAGossip)) {
			gossipePersonToTalkWith.listenToThat(gossip.getLastMessage(), this);
			return gossipePersonToTalkWith;
		}
		return new GossipPerson(Titles.OTHERS, null);
	}

	private boolean isTheListenerIlligibleToHearTheGossip(
			List<GossipPerson> peopleAlreadyHaveAGossip) {
		if (!gossipePersonToTalkWith.amI(Titles.OTHERS) && (gossipePersonToTalkWith.amI(Titles.AGENT)
				|| !peopleAlreadyHaveAGossip.contains(gossipePersonToTalkWith)))
			return true;
		return false;
	}

	public boolean canGossipe() {
		return gossip.hasMessageToSpread();
	}

	public boolean amI(Titles title) {
		return title.equals(this.title);
	}

}
