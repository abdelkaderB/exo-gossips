package com.nespresso.recruitment.gossip;

public class LadyGossipStrategie extends ErrazableGossipStrategie {
	@Override
	public String getLastMessage() {
		if (super.whoTellTheLastMessage.amI(Titles.DR))
			return super.getLastMessage();
		else
			return "";
	}
}
