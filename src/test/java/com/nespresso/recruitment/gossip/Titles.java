package com.nespresso.recruitment.gossip;

public enum Titles {
	MR, DR, SIR, AGENT, PROFESSOR, LADY, OTHERS;
}
