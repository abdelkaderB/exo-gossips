package com.nespresso.recruitment.gossip;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Gossips {
	private final Map<String, GossipPerson> gossipePersones;
	private String wordToSay;

	private GossipPerson gossipPerson;

	public Gossips(String... gossipePersones) {
		this.gossipePersones = createAllGossipePerson(gossipePersones);
	}

	private static Map<String, GossipPerson> createAllGossipePerson(
			String[] gossipePersones2) {
		Map<String, GossipPerson> gossipePersones = new LinkedHashMap<String, GossipPerson>();

		for (String gossipePersone : gossipePersones2) {
			String title = gossipePersone.split(" ")[0];
			String name = gossipePersone.split(" ")[1];
			GossipPerson persone = GosspPeopleFactory.create(title);
			gossipePersones.put(name, persone);
		}
		return gossipePersones;
	}

	public Gossips say(String word) {
		wordToSay = word;
		return this;
	}

	public Gossips from(String name) {
		gossipPerson = gossipePersones.get(name);
		return this;
	}

	public void spread() {
		final List<GossipPerson> peopleThatCanGossip = getAllThatCanGossipe();
		final List<GossipPerson> peopleAlreadyHaveAGossip = new ArrayList<GossipPerson>();
		for (GossipPerson gossipePerson : peopleThatCanGossip) {
			GossipPerson justHadAgossipePerson = gossipePerson.gossipeNow(peopleAlreadyHaveAGossip);
			peopleAlreadyHaveAGossip.add(justHadAgossipePerson);
		}
	}

	private List<GossipPerson> getAllThatCanGossipe() {
		final List<GossipPerson> peopleThatCanGossip = new ArrayList<GossipPerson>();
		final Set<String> names = gossipePersones.keySet();
		for (String name : names) {
			GossipPerson gossipePerson = gossipePersones.get(name);
			if (gossipePerson.canGossipe())
				peopleThatCanGossip.add(gossipePerson);
		}
		return peopleThatCanGossip;
	}

	public String ask(String name) {
		return gossipePersones.get(name).ask();
	}

	public Gossips to(String name) {
		if (gossipPerson != null) {
			gossipPerson.canTalkTo(gossipePersones.get(name));
			gossipPerson = null;
		} else {
			GossipPerson otherPerson = new GossipPerson(Titles.OTHERS, null);
			gossipePersones.get(name).listenToThat(wordToSay, otherPerson);
		}
		return this;
	}

}
