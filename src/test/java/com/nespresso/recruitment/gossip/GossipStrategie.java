package com.nespresso.recruitment.gossip;

public interface GossipStrategie {
	
	public void memorizeThis(String message, GossipPerson from);
	
	public String getAllRetainedMessages();
	
	public String getLastMessage();

	public boolean hasMessageToSpread();
}
