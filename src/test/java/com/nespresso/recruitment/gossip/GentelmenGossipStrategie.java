package com.nespresso.recruitment.gossip;

public class GentelmenGossipStrategie extends ErrazableGossipStrategie {
	@Override
	public String getLastMessage() {
		return getReversedMessage();
	}

	private String getReversedMessage() {
		final StringBuilder reversedMessageBuilder = new StringBuilder();
		final String lastMessage = super.getLastMessage();
		for (int i = lastMessage.length() - 1; i >= 0; i--) {
			reversedMessageBuilder.append(lastMessage.charAt(i));
		}
		return reversedMessageBuilder.toString();
	}
}
