package com.nespresso.recruitment.gossip;

public class ErrazableGossipStrategie implements GossipStrategie{
	private String message;
	protected GossipPerson whoTellTheLastMessage;
	
	public ErrazableGossipStrategie() {
		this.message = "";
	}
	@Override
	public String getAllRetainedMessages() {
		return this.message;
	}
	@Override
	public String getLastMessage() {
		String messageBeforeArraze = this.message.toString();
		this.message="";
		return messageBeforeArraze;
	}
	@Override
	public boolean hasMessageToSpread() {
		return !this.message.equals("");
	}
	@Override
	public void memorizeThis(String message, GossipPerson from) {
		whoTellTheLastMessage=from;
		this.message=message;		
	}
	public void errazeMemory() {
		this.message = "";		
	}

}
